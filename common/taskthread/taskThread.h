
#ifndef TASK_THREAD_H__
#define TASK_THREAD_H__

#include "taskObject.h"
#include <future>

class TaskResultHash;
class TaskQueue;
class TaskUnit;

class TaskThread : public TaskObject
{
private:
	using task_result_ty = std::unique_ptr<TaskResultHash>;
	std::unique_ptr<std::future<task_result_ty>> m_returnTask;

public:
	TaskThread();
	~TaskThread() override;

private:
	virtual bool beforeExec(std::shared_ptr<TaskQueue> queue)
	{
		return true;
	}

protected:
	virtual void doTask(TaskUnit &task);

private:
	virtual void afterExec(TaskResultHash &resultMap) noexcept
	{ }
	void popTaskQueue(TaskResultHash &resultMap, TaskQueue &queue);
	task_result_ty doPerform(std::shared_ptr<TaskQueue> queue);

public:
	void Perform(std::shared_ptr<TaskQueue> queue);
	task_result_ty GetResult();
	void Wait();

private:
	std::mutex m_lock;
};

#endif

