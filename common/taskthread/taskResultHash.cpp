
#include "taskResultHash.h"
#include "taskResult.h"

TaskResultHash::TaskResultHash()
	: TaskObject()
{ }

TaskResultHash::~TaskResultHash()
{ }

void TaskResultHash::Put(const std::string &key, std::unique_ptr<TaskResult> res)
{
	if (key.empty())
		throw std::exception("emtpy key");

	m_rets[key] = std::move(res);
}

void TaskResultHash::Preorder(std::function<void(TaskResult &)> &&fn)
{
	for (auto &task : m_rets)
		fn(*task.second);
}

std::unique_ptr<TaskResult> TaskResultHash::Pop()
{
	if (m_rets.empty())
		return {};

	auto firstIterator = m_rets.begin();
	std::unique_ptr<TaskResult> ret = std::move(firstIterator->second);

	m_rets.erase(firstIterator);
	return ret;
}

void TaskResultHash::Merge(TaskResultHash &other)
{
	for (auto &elem : other.m_rets)
		m_rets[elem.first] = std::move(elem.second);
	other.m_rets.clear();
}
