
#ifndef TASK_QUEUE_H__
#define TASK_QUEUE_H__

#include "common/ccobject/ccobject.h"

class TaskUnit;

class TaskQueue : public CCObject
{
private:
	std::list<std::unique_ptr<TaskUnit>> m_queue;

public:
	TaskQueue();
	~TaskQueue() override;

	void Push(std::unique_ptr<TaskUnit> task);
	std::unique_ptr<TaskUnit> Pop();

private:
	std::mutex m_lock;
};

#endif
