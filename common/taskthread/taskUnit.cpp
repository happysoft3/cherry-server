
#include "taskunit.h"
#include "utils/stringhelper.h"

using namespace _StringHelper;

class AtomicIndex
{
	static constexpr int index_limit = 131072;
	struct IndexTable
	{
		int m_first;
		int m_secondary;
		int m_third;
	};
private:
	std::mutex m_lock;
	std::unique_ptr<IndexTable> m_indexes;

public:
	AtomicIndex()
		: m_indexes(new IndexTable{})
	{ }

private:
	IndexTable makeTable() const
	{
		return { m_indexes->m_first, m_indexes->m_secondary, m_indexes->m_third };
	}
	void computeIndex()
	{
		std::lock_guard<std::mutex> guard(m_lock);
		if (++m_indexes->m_first >= index_limit)
		{
			m_indexes->m_first = 0;
			if (++m_indexes->m_secondary >= index_limit)
			{
				m_indexes->m_secondary = 0;
				if (++m_indexes->m_third >= index_limit)
					m_indexes->m_third = 0;
			}
		}
	}
	
public:
	std::string Pull()
	{
		computeIndex();
		IndexTable indexes = makeTable();

		return stringFormat("%08d:%08d:%08d", indexes.m_first, indexes.m_secondary, indexes.m_third);
	}
};

static AtomicIndex s_indexGen;

TaskUnit::TaskUnit(std::weak_ptr<TaskUnit::Controller> controller)
	: TaskObject()
{
	m_index = s_indexGen.Pull();
	m_taskControl = controller;
}

TaskUnit::~TaskUnit()
{ }

