
#ifndef TASK_OBJECT_H__
#define TASK_OBJECT_H__

#include "common/ccobject/ccobject.h"

class TaskObject : public CCObject
{
private:
	std::unique_ptr<std::exception> m_logicFailure;

public:
	TaskObject(CCObject *parent = nullptr);
	~TaskObject() override;

	void PutError(std::exception &failure);
	template <class ExceptionTy, class... Args>
	void EmplaceError(Args&& ...args)
	{
		m_logicFailure = std::make_unique<ExceptionTy>(std::forward<Args>(args)...);
	}
	std::unique_ptr<std::exception> ReleaseError();
	std::unique_ptr<std::exception> GetError() const;
};

#endif
