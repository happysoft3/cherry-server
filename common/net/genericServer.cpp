
#include "genericServer.h"
#include "socketList.h"
#include "clientUserTable.h"
#include "clientUser.h"
#include "netTcpSocket.h"
#include "utils/myDebug.h"
#include <WS2tcpip.h>
#include <thread>
//#include <MSWSock.h>
#include "netOverlapped.h"
#include "netServerResult.h"

struct GenericServer::Core
{
    HANDLE m_cpHandle;
};

GenericServer::GenericServer()
    : WorkThreadTask(), m_servCore(new Core{ })
{
    m_socketList = std::make_unique<SocketList>();
    m_userTable = std::make_unique<ClientUserTable>();
    m_servOverl = std::make_unique<NetOverlapped>();
}

GenericServer::~GenericServer()
{ }

void GenericServer::initializeClientSocket()
{
    m_socketList->InitialCreate(1000);
    serverDoAccept(nullptr);
}

void GenericServer::initializeServerSocket()
{
    std::unique_ptr<NetSocket> servSock = std::make_unique<NetTcpSocket>();

    if (!servSock->IsValid())
        MY_THROW() << "invalid socket";

    if (!servSock->PutParams(m_ip, m_port))
        MY_THROW() << "invalid ip or port";

    if (!servSock->Bind())
        MY_THROW() << "server socket epic fail-1";

    if (!servSock->Listen())
        MY_THROW() << "server socket epic fail-2";
    
    m_servSocket = std::move(servSock);
}

void GenericServer::initializeServerCompletionPort()
{
    HANDLE hIO = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, nullptr, 0, 2);

    if (!hIO)
        MY_THROW() << "createiocompletionport functions fails, the function returns null";
    m_servCore->m_cpHandle = hIO;

    hIO = ::CreateIoCompletionPort(reinterpret_cast<HANDLE>(**m_servSocket), m_servCore->m_cpHandle, (ULONG_PTR)0, 0);
    if (hIO == nullptr)
        MY_THROW() << "createiocompletionport fails-2";
}

bool GenericServer::initializeServer()
{
    auto res = makeResult<NetServerResult>();

    try
    {
        initializeServerSocket();
        initializeServerCompletionPort();
        initializeClientSocket();
    }
    catch (const std::exception &e)
    {
        MY_PRINT() << e.what();
        static_cast<NetServerResult *>(res)->PutStateMessage(e.what());
        //halt();
        return false;
    }
    return true;
}

void GenericServer::disconnectUser(ClientUser &user)
{
    m_socketList->Push(user.ReleaseSocket());
}

void GenericServer::receiveFromUser(ClientUser &user)
{
    //
}

void GenericServer::serverDoAccept(NetOverlappedObject *pObject, bool quite)
{
    auto clientSock = m_socketList->Pop();

    if (!clientSock)
    {
        if (quite)
            return; //임시 방편... -- 더 이상 참여할 수 없는 상태임 (소켓 한도 초과)
        MY_THROW() << "cannot more create a socket";
    }

    m_servSocket->PostAccept(clientSock.get());
    m_lastSocket = std::move(clientSock);
}

void GenericServer::serverAccept(NetOverlappedObject *pObject, NetSocket *pSocket)
{
    if (m_lastSocket.get() != pSocket)
        return; //TODO. --THIS IS AN ERROR---
    
    //TODO. 여기에서 클라이언트 Accept 작업을 수행합니다
    std::unique_ptr<NetSocket> sock = std::move(m_lastSocket);
    auto user = std::make_unique<ClientUser>();

    if (!::CreateIoCompletionPort(reinterpret_cast<HANDLE>(**sock), m_servCore->m_cpHandle, reinterpret_cast<ULONG_PTR>(user.get()), 0))
    {
        disconnectUser(*user);
        return; //TODO. --THIS IS AN ERROR--- 연결 끊어버리면 될듯
    }
    sock->PostReceive();   //sock->PostReceive()
    user->AttachSocket(std::move(sock));
    m_userTable->Add(std::move(user));
    serverDoAccept(pObject, true);
}

void GenericServer::serverReceive(bool bResult, NetSocket *pSocket, NetOverlappedObject *pObject, uint32_t uPostBytes)
{
    if ((!bResult) || (!uPostBytes))
    {
        //Disconnected
        auto user = m_userTable->Remove(*pSocket);

        if (user)  //@brief. 연결 끊겼을 때, 소켓 반환
        {
            disconnectUser(*user);
        }
        return;
    }
    //@brief. 여기에서, Overlapped 버퍼에서 UserTable 버퍼로 옮겨적음//
    ClientUser *user = m_userTable->GetUser(*pSocket);

    user->ReceiveData(*pObject); //구현필요
    pSocket->PostReceive(pObject);
}

void GenericServer::serverSend(NetOverlappedObject *pObject, NetSocket *pSocket)
{
    //전송(서버->클라이언트) //예를 들어, 128 한도이면, 256 보낼 때, 2개 버퍼로 나누어서 전송할 것

    ClientUser *pUser = m_userTable->GetUser(*pSocket);

    if (pUser)
    {
        pUser->SendPost(*pObject);
    }
}

void GenericServer::SetIpPort(const std::string &ip, uint32_t port)
{
    m_ip = ip;
    m_port = port;
}

void GenericServer::Startup()
{
    if (!initializeServer())
        return;
}

void GenericServer::serverWork()
{
    uint32_t bytes = 0;
    NetSocket *clSocket = nullptr;
    LPOVERLAPPED pOverl = nullptr;

    bool res = ::GetQueuedCompletionStatus(
        m_servCore->m_cpHandle, reinterpret_cast<DWORD *>(&bytes), reinterpret_cast<LPDWORD>(&clSocket), &pOverl, INFINITE) & true;

    if (pOverl == nullptr)
        return;

    NetOverlappedObject *pObject = NetOverlappedObject::ToObject(pOverl, &bytes);

    switch (pObject->GetIOType())
    {
    case NetOverlappedObject::IOType::ACCEPT:
        serverAccept(pObject, clSocket);
        break;

    case NetOverlappedObject::IOType::RECEIVE:
        serverReceive(res, clSocket, pObject, bytes);
        break;

    case NetOverlappedObject::IOType::SEND:
        serverSend(pObject, clSocket);
        break;

    case NetOverlappedObject::IOType::SHUTDOWN:
        return;
    }
}

void GenericServer::stopService()
{
    NetOverlappedObject *pObject = m_servOverl->Create(NetOverlappedObject::IOType::SHUTDOWN);

    ::PostQueuedCompletionStatus(m_servCore->m_cpHandle, 0, 0, reinterpret_cast<LPOVERLAPPED>( pObject->GetRaw()) );
}

void GenericServer::runTask() //여기에서 overlapped 작업을 수행합니다
{
    serverWork();
}



