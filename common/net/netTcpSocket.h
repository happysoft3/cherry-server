
#ifndef NET_TCP_SOCKET_H__
#define NET_TCP_SOCKET_H__

#include "netSocket.h"

class NetTcpSocket : public NetSocket
{
public:
    NetTcpSocket();
    ~NetTcpSocket() override;
};

#endif

