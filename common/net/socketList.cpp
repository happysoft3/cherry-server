
#include "socketList.h"
#include "netTcpSocket.h"

SocketList::SocketList()
{ }

SocketList::~SocketList()
{ }

int SocketList::loadValidCount(uint32_t input) const
{
    int c = input & 0x7ff;

    switch (c)
    {
    case 0: case 1: case 2: case 3: case 4: case 5:
    case 6: case 7: case 8: case 9:
        return 10; //minimum
    }
    return c;
}

void SocketList::InitialCreate(uint32_t nCounts)
{
    if (m_list.empty())
    {
        int n = loadValidCount(nCounts);

        while (--n >= 0)
            m_list.push_back(std::make_unique<NetTcpSocket>());
    }
}

std::unique_ptr<NetSocket> SocketList::Pop()
{
    if (m_list.empty())
        return{ };

    std::unique_ptr<NetSocket> s = std::move(m_list.front() );

    m_list.pop_front();
    return s;
}

void SocketList::Push(std::unique_ptr<NetSocket> s)
{
    if (s)
        m_list.push_back(std::move(s));
}

