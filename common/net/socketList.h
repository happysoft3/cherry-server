
#ifndef SOCKET_LIST_H__
#define SOCKET_LIST_H__

#include <list>
#include <memory>

class NetSocket;

class SocketList
{
private:
    std::list<std::unique_ptr<NetSocket>> m_list;

public:
    SocketList();
    ~SocketList();

private:
    int loadValidCount(uint32_t input) const;

public:
    void InitialCreate(uint32_t nCounts);
    std::unique_ptr<NetSocket> Pop();
    void Push(std::unique_ptr<NetSocket> s);
};

#endif

