
#ifndef CLIENT_USER_TABLE_H__
#define CLIENT_USER_TABLE_H__

#include <memory>
#include <list>
#include <map>
#include <mutex>

class ClientUser;
class NetSocket;

class ClientUserTable
{
    using socket_list_ty = std::list<std::unique_ptr<ClientUser>>;
private:
    socket_list_ty m_users;
    std::map<uint32_t, socket_list_ty::iterator> m_iterMap;
    socket_list_ty::iterator m_lastIterator;

public:
    ClientUserTable();
    ~ClientUserTable();

public:
    ClientUser *GetUser(NetSocket &sock);
    void Add(std::unique_ptr<ClientUser> user, bool forced = false);
    std::unique_ptr<ClientUser> Remove(NetSocket &sock);

private:
    std::mutex m_lock;
};

#endif

