
#include "serverLauncher.h"
#include "utils/workThreadManager.h"
#include "utils/workThreadTask.h"

ServerLauncher::ServerLauncher()
{
    m_workMan = std::make_unique<WorkThreadManager>();
}

ServerLauncher::~ServerLauncher()
{ }

void ServerLauncher::Launch(std::unique_ptr<WorkThreadTask> task)
{
    if (task)
        m_workMan->Deploy(std::move(task));
} //여기에서 워크스레드 매니저 발동

void ServerLauncher::Shutdown()
{
    m_workMan->Stop();
}





