
#ifndef NETWORK_INSTANCE_H__
#define NETWORK_INSTANCE_H__

#include <memory>

class NetworkInstance
{
    struct Core;

private:
    std::unique_ptr<Core> m_core;

public:
    NetworkInstance();
    ~NetworkInstance();

private:
    void initWsa();
};

#endif

