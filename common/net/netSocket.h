
#ifndef NET_SOCKET_H__
#define NET_SOCKET_H__

#include <string>
#include <memory>
#include <mutex>

class NetOverlapped;
class NetOverlappedObject;
class StreamBuffer;

class NetSocket
{
    class WrappedFd;
    struct Core;

private:
    std::unique_ptr<WrappedFd> m_socket;
    std::unique_ptr<Core> m_core;
    std::unique_ptr<NetOverlapped> m_acceptOverl;
    std::unique_ptr<NetOverlapped> m_recvOverl;
    std::unique_ptr<NetOverlapped> m_sendOverl;

public:
    explicit NetSocket(uint32_t sock);
    virtual ~NetSocket();

private:
    void settingParams(const std::string &ip, uint32_t port);

public:
    uint32_t operator*() const;
    bool IsValid() const;
    bool PutParams(const std::string &ip, uint32_t port);
    bool Bind();
    bool Listen(int backlog = 5);
    void PostAccept(NetSocket *pClientSocket, NetOverlappedObject *pObject = nullptr);

private:
    void postReceiveImpl();

public:
    void PostReceive(NetOverlappedObject *pObject = nullptr);
    void Disconnect();

private:
    void sendImpl(StreamBuffer &buff);

public:
    bool SendBuffer(StreamBuffer &buff);
    void PostSend(NetOverlappedObject &overl, StreamBuffer &pBuffer);

private:
    std::mutex m_sendLock;
};

#endif

