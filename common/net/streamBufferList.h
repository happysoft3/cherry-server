
#ifndef STREAM_BUFFER_LIST_H__
#define STREAM_BUFFER_LIST_H__

#include <list>
#include <memory>
#include <mutex>

class StreamBuffer;

class StreamBufferList
{
private:
    std::list<std::unique_ptr<StreamBuffer>> m_buffers;

public:
    StreamBufferList();
    ~StreamBufferList();

public:
    void PushBack(std::unique_ptr<StreamBuffer> buff);
    void PushFront(std::unique_ptr<StreamBuffer> buff);
    std::unique_ptr<StreamBuffer> PopFront();
    void PushBackPartition(const StreamBuffer &src, size_t limit);

private:
    std::mutex m_lock;
};

#endif

