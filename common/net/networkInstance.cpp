
#include "networkInstance.h"
#include <WS2tcpip.h>
#include <functional>

static NetworkInstance s_netInstance;

using wsa_ptr_ty = std::unique_ptr<WSADATA, std::function<void(WSADATA*)>>;
static auto s_wsa_destructor = [](WSADATA *wsa) { ::WSACleanup(); delete wsa; };

struct NetworkInstance::Core
{
    wsa_ptr_ty m_wsa;
};

NetworkInstance::NetworkInstance()
{
    m_core = std::make_unique<Core>();

    initWsa();
    
}

NetworkInstance::~NetworkInstance()
{ }

void NetworkInstance::initWsa()
{
    std::unique_ptr<WSADATA> wsadata(new WSADATA);

    if (::WSAStartup(MAKEWORD(2, 2), wsadata.get()) != 0)
    {
        //wsa 초기화오류
        return;
    }
    wsa_ptr_ty pWsa(wsadata.release(), s_wsa_destructor);

    m_core->m_wsa = std::move(pWsa);
}
