
#ifndef NET_OVERLAPPED_H__
#define NET_OVERLAPPED_H__

#include <memory>
#include <list>
#include <map>
#include <string>

class NetSocket;
class NetOverlappedObject;
class StreamBuffer;

constexpr size_t net_buffer_max_size = 64;
class NetOverlappedObject
{
    struct Core;

public:
    enum class IOType
    {
        NONE,
        ACCEPT,
        RECEIVE,
        SEND,
        SHUTDOWN,
    };

private:
    std::unique_ptr<Core> m_core;
    uint32_t m_bytes;
    uint32_t m_postBytes;
    uint32_t m_dwFlags;
    IOType m_ioType;

public:
    explicit NetOverlappedObject(IOType ty = IOType::NONE);
    ~NetOverlappedObject();

public:
    IOType GetIOType() const
    {
        return m_ioType;
    }

public:
    void ClearBuffer();
    void StoreBuffer(const std::string &src);
    void GetBuffer(std::unique_ptr<StreamBuffer> &buff);

    using lp_overlapped_ty = void*;
    static NetOverlappedObject *ToObject(lp_overlapped_ty pOverl, uint32_t *pBytes = nullptr);
    lp_overlapped_ty GetRaw() const;
    int ReserveRecv(NetSocket &sock);
    int ReserveSend(NetSocket &sock, StreamBuffer *pBuffer = nullptr);
};

class NetOverlapped
{
    using overlapped_elem_ty = std::unique_ptr<NetOverlappedObject>;
    using overlapped_list_iter_ty = std::list<overlapped_elem_ty>::iterator;

private:
    std::map<NetOverlappedObject *, overlapped_list_iter_ty> m_overlListIterMap;
    std::list<overlapped_elem_ty> m_overlappedList;

public:
    NetOverlapped();
    ~NetOverlapped();

private:
    NetOverlappedObject *createCommon(NetOverlappedObject::IOType ioType);

public:
    NetOverlappedObject *Create(NetOverlappedObject::IOType ioType);
    NetOverlappedObject *CreateWithBuffer(const std::string &buffer, NetOverlappedObject::IOType ioType);

private:
    overlapped_list_iter_ty getIteratorFromMap(NetOverlappedObject *pOverl);

public:
    bool Delete(NetOverlappedObject *pOverl);
    size_t Count() const;
};

#endif


