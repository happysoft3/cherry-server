
#ifndef SERVER_LAUNCHER_H__
#define SERVER_LAUNCHER_H__

#include <memory>

class WorkThreadManager;
class WorkThreadTask;

class ServerLauncher
{
private:
    std::unique_ptr<WorkThreadManager> m_workMan;

public:
    ServerLauncher();
    ~ServerLauncher();

public:
    void Launch(std::unique_ptr<WorkThreadTask> task);
    void Shutdown();
};

#endif

