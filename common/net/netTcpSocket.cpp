
#include "netTcpSocket.h"
#include <WS2tcpip.h>

NetTcpSocket::NetTcpSocket()
    : NetSocket(::WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED))
{ }

NetTcpSocket::~NetTcpSocket()
{ }
