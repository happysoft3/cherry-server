
#ifndef CLIENT_USER_H__
#define CLIENT_USER_H__

#include <vector>
#include <memory>

class NetSocket;
class StreamBufferList;
class NetOverlappedObject;

class ClientUser
{
private:
    std::unique_ptr<NetSocket> m_sock;
    std::unique_ptr<StreamBufferList> m_bufferList;
    std::unique_ptr<StreamBufferList> m_sendBufferList;

public:
    ClientUser();
    ~ClientUser();

public:
    void AttachSocket(std::unique_ptr<NetSocket> sock);
    std::unique_ptr<NetSocket> ReleaseSocket();
    uint32_t UserID() const;
    void ReceiveData(NetOverlappedObject &overlObject);
    void SendData(const std::vector<uint8_t> &data);
    void SendPost(NetOverlappedObject &overlObject);
};

#endif

