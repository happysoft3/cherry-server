
#ifndef NET_SERVER_RESULT_H__
#define NET_SERVER_RESULT_H__

#include "utils/workTaskResult.h"
#include <string>

class NetServerResult : public WorkTaskResult
{
private:
    std::string m_readable;

public:
    NetServerResult();
    ~NetServerResult() override;

public:
    void PutStateMessage(const std::string &message)
    {
        m_readable = message;
    }
};

#endif

