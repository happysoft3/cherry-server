
#include "netoverlapped.h"
#include "netSocket.h"
#include "streamBuffer.h"
#include <WS2tcpip.h>
#include <array>

/////// new implementations /////////////

struct NetOverlappedObject::Core : public WSAOVERLAPPED
{
    WSABUF m_wsaBuffer;
    std::unique_ptr<StreamBuffer> m_stream;
    NetOverlappedObject *m_pObject;
};

NetOverlappedObject::NetOverlappedObject(NetOverlappedObject::IOType ty)
    : m_ioType(ty)
{
    m_core = std::unique_ptr<Core>(new Core{ });
    auto stream = std::make_unique<StreamBuffer>();

    stream->Resize(net_buffer_max_size);
    m_core->m_wsaBuffer.buf = reinterpret_cast<char *>( stream->GetRaw() );
    m_core->m_wsaBuffer.len = net_buffer_max_size;
    m_core->m_stream = std::move( stream );
    m_core->m_pObject = this;
    m_bytes = 0;
    m_postBytes = 0;
}

NetOverlappedObject::~NetOverlappedObject()
{ }

void NetOverlappedObject::ClearBuffer()
{
    m_core->m_stream->Fill(0);
}

void NetOverlappedObject::StoreBuffer(const std::string &src)
{
    m_core->m_stream->Dump(src);
    m_core->m_wsaBuffer.len = src.length();
}

void NetOverlappedObject::GetBuffer(std::unique_ptr<StreamBuffer> &buff)
{
    buff = m_core->m_stream->CloneBuffer();
}

NetOverlappedObject *NetOverlappedObject::ToObject(lp_overlapped_ty pOverl, uint32_t *pBytes)
{
    if (!pOverl)
        return nullptr;

    Core *pCore = reinterpret_cast<Core *>(pOverl);

    if (pBytes)
        pCore->m_pObject->m_postBytes = *pBytes;
    return pCore->m_pObject;
}

NetOverlappedObject::lp_overlapped_ty NetOverlappedObject::GetRaw() const
{
    return m_core.get();
}

int NetOverlappedObject::ReserveRecv(NetSocket &sock)
{
    return ::WSARecv(
        *sock, &m_core->m_wsaBuffer, 1, 
        reinterpret_cast<LPDWORD>(&m_bytes), 
        reinterpret_cast<LPDWORD>(&m_dwFlags), 
        reinterpret_cast<LPWSAOVERLAPPED>(m_core.get()), nullptr);
}

int NetOverlappedObject::ReserveSend(NetSocket &sock, StreamBuffer *pBuffer)
{
    if (pBuffer)
        m_core->m_stream->Dump(*pBuffer);

    return ::WSASend(*sock, &m_core->m_wsaBuffer, 1,
        reinterpret_cast<LPDWORD>(&m_bytes),
        m_dwFlags,
        reinterpret_cast<LPWSAOVERLAPPED>(m_core.get()), nullptr);
}

///////////

NetOverlapped::NetOverlapped()
{ }

NetOverlapped::~NetOverlapped()
{ }

NetOverlappedObject *NetOverlapped::createCommon(NetOverlappedObject::IOType ioType)
{
    NetOverlappedObject *pOverl = new NetOverlappedObject(ioType);
    auto listIter = m_overlappedList.emplace(m_overlappedList.end(), pOverl);

    m_overlListIterMap[pOverl] = listIter;
    return pOverl;
}

NetOverlappedObject *NetOverlapped::Create(NetOverlappedObject::IOType ioType)
{
    NetOverlappedObject *pOverl = createCommon(ioType);

    pOverl->ClearBuffer();
    return pOverl;
}

NetOverlappedObject *NetOverlapped::CreateWithBuffer(const std::string &buffer, NetOverlappedObject::IOType ioType)
{
    NetOverlappedObject *pOverl = createCommon(ioType);

    pOverl->StoreBuffer(buffer);
    return pOverl;
}

NetOverlapped::overlapped_list_iter_ty NetOverlapped::getIteratorFromMap(NetOverlappedObject *pOverl)
{
    auto keyIterator = m_overlListIterMap.find(pOverl);

    if (keyIterator != m_overlListIterMap.cend())
    {
        auto listIterator = keyIterator->second;

        m_overlListIterMap.erase(keyIterator);
        return listIterator;
    }

    return m_overlappedList.end();
}

bool NetOverlapped::Delete(NetOverlappedObject *pOverl)
{
    if (m_overlappedList.empty())
        return false;

    auto listIterator = getIteratorFromMap(pOverl);

    if (listIterator == m_overlappedList.cend())
        return false;

    m_overlappedList.erase(listIterator);
    return true;
}

size_t NetOverlapped::Count() const
{
    return m_overlappedList.size();
}

