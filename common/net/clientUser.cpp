
#include "clientUser.h"
#include "netSocket.h"
#include "netOverlapped.h"
#include "streamBufferList.h"
#include "streamBuffer.h"

ClientUser::ClientUser()
{
    m_bufferList = std::make_unique<StreamBufferList>();
    m_sendBufferList = std::make_unique<StreamBufferList>();
}

ClientUser::~ClientUser()
{ }

void ClientUser::AttachSocket(std::unique_ptr<NetSocket> sock)
{
    m_sock = std::move(sock);
}

std::unique_ptr<NetSocket> ClientUser::ReleaseSocket()
{
    if (!m_sock)
        return{ };

    m_sock->Disconnect();
    return std::move(m_sock);
}

uint32_t ClientUser::UserID() const
{
    if (!m_sock)
        return 0;

    return **m_sock;
}

void ClientUser::ReceiveData(NetOverlappedObject &overlObject)
{
    ///overl 으로 부터 버퍼 내용을 받아서, 클라이언트 버퍼 리스트에 추가한다
    std::unique_ptr<StreamBuffer> buff;

    overlObject.GetBuffer(buff);
    m_bufferList->PushBack(std::move(buff));
}

void ClientUser::SendData(const std::vector<uint8_t> &data)
{
    if (data.empty())
        return;
    if (!m_sock)
        return;

    StreamBuffer buffer;

    buffer.Resize(data.size());
    buffer.Dump(data);
    m_sendBufferList->PushBackPartition(buffer, net_buffer_max_size);
    std::unique_ptr<StreamBuffer> first = m_sendBufferList->PopFront();

    if (!m_sock->SendBuffer(*first)) //Todo. 여기에서 소켓에 wsasend 한다
        m_sendBufferList->PushFront(std::move(first));
}

void ClientUser::SendPost(NetOverlappedObject &overlObject)
{
    std::unique_ptr<StreamBuffer> buff = m_sendBufferList->PopFront();

    if (!buff)
        return;

    m_sock->PostSend(overlObject, *buff);
}
