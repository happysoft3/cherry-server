
#include "streamBuffer.h"

StreamBuffer::StreamBuffer()
{ }

StreamBuffer::~StreamBuffer()
{ }

void StreamBuffer::Resize(size_t sz)
{
    if (sz == 0)
    {
        if (m_stream.size())
        {
            m_stream.clear();
            return;
        }
    }
    if (m_stream.size() != sz)
        m_stream.resize(sz);
}

uint8_t *StreamBuffer::GetRaw()
{
    if (m_stream.empty())
        return nullptr;

    return m_stream.data();
}

void StreamBuffer::Fill(uint8_t n)
{
    for (auto &e : m_stream)
        e = n;
}

void StreamBuffer::Dump(const StreamBuffer &buff)
{
    m_stream = buff.m_stream;
}

std::unique_ptr<StreamBuffer> StreamBuffer::CloneBuffer() const
{
    auto clone = std::make_unique<StreamBuffer>();

    *clone = *this;
    return clone;
}

size_t StreamBuffer::SizedCopy(const StreamBuffer &src, size_t limit, size_t nIndex)
{
    const std::vector<uint8_t> &srcStream = src.m_stream;
    std::vector<uint8_t> dest(limit, 0);
    size_t max = srcStream.size();
    int workPos = -1;

    while (++workPos < limit)
    {
        if (nIndex >= max)
            break;

        dest[workPos] = srcStream[nIndex++];
    }
    if (workPos != limit)
        dest.resize(workPos);
    m_stream = dest;
    return workPos;
}
