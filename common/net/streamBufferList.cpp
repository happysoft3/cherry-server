
#include "streamBufferlist.h"
#include "streamBuffer.h"

StreamBufferList::StreamBufferList()
{ }

StreamBufferList::~StreamBufferList()
{ }

void StreamBufferList::PushBack(std::unique_ptr<StreamBuffer> buff)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_buffers.push_back(std::move(buff));
}

void StreamBufferList::PushFront(std::unique_ptr<StreamBuffer> buff)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_buffers.push_front(std::move(buff));
}

std::unique_ptr<StreamBuffer> StreamBufferList::PopFront()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_buffers.empty())
        return{ };

    auto first = std::move( m_buffers.front() );

    m_buffers.pop_front();
    return first;
}

void StreamBufferList::PushBackPartition(const StreamBuffer &src, size_t limit)
{
    size_t pos = 0, workPos;
    std::unique_ptr< StreamBuffer > buffer;

    for (;;)
    {
        buffer = std::make_unique<StreamBuffer>();
        workPos = buffer->SizedCopy(src, limit, pos);

        if (workPos != 0)
        {
            std::lock_guard<std::mutex> lock(m_lock);
            m_buffers.push_back(std::move(buffer));
        }

        if (workPos != limit)
            break;
        pos += limit;
    }
}
