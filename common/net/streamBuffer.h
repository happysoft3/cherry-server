
#ifndef STREAM_BUFFER_H__
#define STREAM_BUFFER_H__

#include <vector>
#include <memory>

class StreamBuffer
{
private:
    std::vector<uint8_t> m_stream;

public:
    StreamBuffer();
    ~StreamBuffer();

public:
    void Resize(size_t sz);
    uint8_t *GetRaw();
    void Fill(uint8_t n);

    template <class Container>
    void Dump(const Container &cont)
    {
        auto iter = cont.cbegin();

        for (auto &e : m_stream)
        {
            if (iter == cont.cend())
                return;
            e = *iter;
        }
    }
    void Dump(const StreamBuffer &buff);
    std::unique_ptr<StreamBuffer> CloneBuffer() const;
    size_t SizedCopy(const StreamBuffer &src, size_t limit, size_t nIndex);
};

#endif

