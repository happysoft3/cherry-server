
#include "clientUserTable.h"
#include "clientUser.h"
#include "netSocket.h"

ClientUserTable::ClientUserTable()
{ }

ClientUserTable::~ClientUserTable()
{ }

ClientUser *ClientUserTable::GetUser(NetSocket &sock)
{
    std::lock_guard<std::mutex> lock(m_lock);
    auto iter = m_iterMap.find(*sock);

    if (iter == m_iterMap.cend())
        return nullptr;

    return iter->second->get();
}

void ClientUserTable::Add(std::unique_ptr<ClientUser> user, bool forced)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_iterMap.find(user->UserID()) == m_iterMap.cend())
    {
        m_users.push_back(std::move(user));
        m_iterMap[user->UserID()] = std::prev(m_users.end());
    }
}

std::unique_ptr<ClientUser> ClientUserTable::Remove(NetSocket &sock)
{
    std::lock_guard<std::mutex> lock(m_lock);
    auto iter = m_iterMap.find(*sock);

    if (iter != m_iterMap.cend())
    {
        auto user = std::move( *iter->second );

        m_users.erase(iter->second);
        m_iterMap.erase(iter);
        return user;
    }
    return{ };
}


