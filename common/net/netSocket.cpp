
#include "netSocket.h"
#include "netOverlapped.h"
#include "streamBufferList.h"
#include "streamBuffer.h"
#include "utils/iptobyte.h"
#include <iterator>
#include <WS2tcpip.h>
#include <MSWSock.h>

class NetSocket::WrappedFd
{
private:
    uint32_t m_fd;

public:
    explicit WrappedFd(uint32_t sock=INVALID_SOCKET)
        : m_fd(sock)
    { }
    ~WrappedFd()
    {
        Close();
    }
    uint32_t operator*() const
    {
        return m_fd;
    }
    bool Valid() const
    {
        return m_fd != INVALID_SOCKET;
    }
    void Reset()
    {
        m_fd = INVALID_SOCKET;
    }
    void Close()
    {
        if (m_fd != INVALID_SOCKET)
            ::closesocket(m_fd);
        Reset();
    }
};

struct NetSocket::Core
{
    std::unique_ptr<SOCKADDR_IN> m_addrIn;
    std::vector<uint8_t> m_acceptBuffer;
    size_t m_acceptBytes;
};

NetSocket::NetSocket(uint32_t sock)
    : m_core(new Core({ }))
{
    m_socket = std::make_unique<WrappedFd>(sock);
    m_acceptOverl = std::make_unique<NetOverlapped>();
    m_recvOverl = std::make_unique<NetOverlapped>();
    m_sendOverl = std::make_unique<NetOverlapped>();
}

NetSocket::~NetSocket()
{ }

void NetSocket::settingParams(const std::string &ip, uint32_t port)
{
    IpToByte ipconv;

    ipconv.SetIpAddress(ip);
    m_core->m_addrIn = std::make_unique<SOCKADDR_IN>();

    m_core->m_addrIn->sin_family = AF_INET;
    m_core->m_addrIn->sin_port = htons(static_cast<u_short>(port));

    std::vector<char> ipbyte;

    ipconv.ToByteStream(ipbyte);
    char *pSinAddr = reinterpret_cast<char *>(&m_core->m_addrIn->sin_addr);
    std::copy(ipbyte.begin(), ipbyte.end(), stdext::checked_array_iterator<char *>(pSinAddr, 4));
}

uint32_t NetSocket::operator*() const
{
    return **m_socket;
}

bool NetSocket::IsValid() const
{
    if (m_socket->Valid())
        return true;

    return false;
}

bool NetSocket::PutParams(const std::string &ip, uint32_t port)
{
    if (!port)
        return false;

    if (ip.empty())
        return false;

    settingParams(ip, port);
    return true;
}

bool NetSocket::Bind()
{
    if (!m_socket->Valid())
        return false;

    if (!m_core->m_addrIn)
        return false;

    if (::bind(**m_socket, reinterpret_cast<sockaddr *>(m_core->m_addrIn.get()), sizeof(SOCKADDR_IN)) != SOCKET_ERROR)
        return true;

    m_socket->Reset();
    return false;
}

bool NetSocket::Listen(int backlog)
{
    if (m_socket->Valid())
    {
        if (::listen(**m_socket, backlog) != SOCKET_ERROR)
            return true;

        m_socket->Reset();
    }
    return false;
}

void NetSocket::PostAccept(NetSocket *pClientSocket, NetOverlappedObject *pObject)
{
    if (!pClientSocket)
        return;

    if (pObject)
        m_acceptOverl->Delete(pObject);

    pObject = m_acceptOverl->Create(NetOverlappedObject::IOType::ACCEPT);

    static constexpr size_t commonAddrLength = sizeof(SOCKADDR_IN) + 16;
    if (::AcceptEx(
        **this,
        **pClientSocket,
        pClientSocket->m_core->m_acceptBuffer.data(),
        0, commonAddrLength, commonAddrLength, reinterpret_cast<LPDWORD>(&pClientSocket->m_core->m_acceptBytes), reinterpret_cast<LPOVERLAPPED>(pObject->GetRaw())))
        return; //Todo. 예외처리

    //pObject->PutOperation(std::forward<post_fn_ty>(fn));
}

void NetSocket::postReceiveImpl()
{
    auto pObject = m_recvOverl->Create(NetOverlappedObject::IOType::RECEIVE);
    int res = pObject->ReserveRecv(*this);

    if (res == SOCKET_ERROR && (::WSAGetLastError() != ERROR_IO_PENDING))
    {
        ///put message. -error- //연결해제 등의 예외처리가 요구될 것으로..
        return;
    }
}

void NetSocket::PostReceive(NetOverlappedObject *pObject)
{
    //Todo. 오버랩드 오브젝트로 부터 버퍼 얻기//
    if (pObject)
        m_recvOverl->Delete(pObject);
    //받기 작업 다시 걸어놓기
    postReceiveImpl();
}

void NetSocket::Disconnect()
{
    if (!m_socket->Valid())
        return;

    m_socket->Close();
}

void NetSocket::sendImpl(StreamBuffer &buff)
{
    NetOverlappedObject *pObject = m_sendOverl->Create(NetOverlappedObject::IOType::SEND);

    pObject->ReserveSend(*this, &buff);
}

bool NetSocket::SendBuffer(StreamBuffer &buff)
{
    std::lock_guard<std::mutex> lock(m_sendLock);
    if (m_sendOverl->Count())
        return false; //이미 작업이 게시됨//

    sendImpl(buff);
    return true;
}

void NetSocket::PostSend(NetOverlappedObject &overl, StreamBuffer &pBuffer)
{
    std::lock_guard<std::mutex> lock(m_sendLock);
    m_sendOverl->Delete(&overl);
    sendImpl(pBuffer);
}

