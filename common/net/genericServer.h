
#ifndef GENERIC_SERVER_H__
#define GENERIC_SERVER_H__

#include "common/utils/workThreadTask.h"
#include <memory>
#include <string>
#include <future>

class NetSocket;
class SocketList;
class ClientUserTable;
class NetOverlappedObject;
class NetOverlapped;

class GenericServer : public WorkThreadTask
{
    struct Core;
private:
    std::unique_ptr<NetSocket> m_servSocket;
    std::string m_ip;
    uint32_t m_port;
    std::unique_ptr<Core> m_servCore;
    std::unique_ptr<SocketList> m_socketList;
    std::unique_ptr<NetSocket> m_lastSocket;
    std::unique_ptr<ClientUserTable> m_userTable;
    std::unique_ptr<NetOverlapped> m_servOverl;

public:
    GenericServer();
    ~GenericServer() override;

private:
    void initializeClientSocket();
    void initializeServerSocket();
    void initializeServerCompletionPort();
    bool initializeServer();
    void disconnectUser(ClientUser &user);
    virtual void receiveFromUser(ClientUser &user);

private:
    void serverDoAccept(NetOverlappedObject *pObject, bool quite = false);
    void serverAccept(NetOverlappedObject *pObject, NetSocket *pSocket);
    void serverReceive(bool bResult, NetSocket *pSocket, NetOverlappedObject *pObject, uint32_t uPostBytes);
    void serverSend(NetOverlappedObject *pObject, NetSocket *pSocket);

public:
    void SetIpPort(const std::string &ip, uint32_t port);
    void Startup();

private:
    void serverWork();
    void stopService() override;
    void runTask() override;
};

#endif

