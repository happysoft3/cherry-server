
#ifndef WORK_THREAD_MANAGER_H__
#define WORK_THREAD_MANAGER_H__

#include <list>
#include <memory>
#include <future>

class WorkThread;
class WorkThreadTask;

class WorkThreadManager
{
private:
    std::shared_ptr<bool> m_actSignal;
    std::list<std::unique_ptr<WorkThread>> m_workers;
    std::future<void> m_workerResult;
    //스레드 하나 띄운다-> 워크스레드 n 개를 생성 -> 스레드가 모든 워크 스레드를 join 한다

public:
    WorkThreadManager();
    ~WorkThreadManager();

private:
    bool confirmConcurrent(size_t n) const;
    void makeWorkerN(size_t n, std::shared_ptr<WorkThreadTask> task);
    void doDeploy();

public:
    void Deploy(std::unique_ptr<WorkThreadTask> task, size_t nConcurrent = 3);
    void Stop();
};

#endif

