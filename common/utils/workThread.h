
#ifndef WORK_THREAD_H__
#define WORK_THREAD_H__

#include <memory>
#include <future>

class WorkThreadTask;

class WorkThread
{
private:
    std::future<int> m_workResult;
    std::weak_ptr<WorkThreadTask> m_taskAlive;
    std::shared_ptr<int> m_alive;

public:
    WorkThread();
    ~WorkThread();

private:
    int doWork(std::shared_ptr<WorkThreadTask> task, std::weak_ptr<bool> key);

public:
    void Run(std::shared_ptr<WorkThreadTask> task, std::weak_ptr<bool> key = { });
    void Wait(); //결과를 반환하던 지, 아무튼 스레드 합류
    void PostStop();
};

#endif

