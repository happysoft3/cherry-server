
#include "workThread.h"
#include "workTaskResult.h"
#include "workThreadTask.h"

WorkThread::WorkThread()
{ }

WorkThread::~WorkThread()
{ }

int WorkThread::doWork(std::shared_ptr<WorkThreadTask> task, std::weak_ptr<bool> key)
{
    task->Run(key);
    return 0;
}

void WorkThread::Run(std::shared_ptr<WorkThreadTask> task, std::weak_ptr<bool> key)
{
    if (!m_taskAlive.expired())
        return;

    m_taskAlive = task;
    m_workResult = std::async(std::launch::async, [this, task, key]() { return this->doWork(task, key); });
}

void WorkThread::Wait()
{
    if (m_workResult.valid())
        m_workResult.get();
}

void WorkThread::PostStop()
{
    auto task = m_taskAlive.lock();

    if (!task)
        return;

    task->Stop();
}

