
#ifndef URL_STRING_H__
#define URL_STRING_H__

#include "binaryHelper.h"
#include <functional>
#include <list>
#include <map>
#include <memory>

class UrlString : public BinaryHelper
{
	class Term;
	class Node;

public:
	using node_value_fn_ty = std::function<std::string()>;
	using node_value_fn_pointer_ty = std::shared_ptr<node_value_fn_ty>;

private:
	////// encode, decode
	size_t m_rdPos;
	std::vector<uint8_t> m_store;

	////// url separator
	std::list<std::unique_ptr<Term>> m_strList;
	std::map<std::string, std::unique_ptr<Node>> m_nodeMap;
	node_value_fn_pointer_ty m_setValueFn;

public:
	UrlString();
	~UrlString() override;
	
private:	//method for decoding
	bool isHexaChar(int c) const;
	bool expectHexa();
	int getHexByte(uint8_t in);
	bool calcDecodeResult(uint32_t low, uint32_t high, uint8_t &outp);
	bool readHexaChar(uint8_t &uc);
	void decodeImpl();

public:
	template <class Container>
	void Decode(Container &dest)
	{
		decodeImpl();
		dest.resize(m_store.size());
		std::copy(m_store.cbegin(), m_store.cend(), dest.begin());
	}

private:	//method for encoding
	template <class Container>
	void moveToStore(const Container &src, const size_t &index = -1);

	bool needEncodeChar(int c) const;
	void encodeImpl(uint32_t c, size_t resz);
	void encodeImplOld();

public:
	template <class Container>
	void Encode(Container &dest)
	{
		encodeImpl(pop(), 0);
		//encodeImplOld();
		dest.resize(m_store.size());
		std::copy(m_store.cbegin(), m_store.cend(), dest.begin());
	}

protected:
	std::unique_ptr<Term> popTerm();
	void pushTerm(std::unique_ptr<Term> term);
	Term *peekTerm() const;

private:
	void splitPath();
	void investigate();
	Node *findNode(const std::string &key);
	std::string readValue();

public:
	void ToNode();
	std::string Value(const std::string &key) const;
	bool HasValue(const std::string &key) const;

protected:
	void putValueFunction(const std::string &key, node_value_fn_ty &&fn);
};

////////////////////////////////////////////////////////////////////////////////////
class UrlString::Term
{
private:
	std::string m_sValue;
	bool m_empty;

public:
	explicit Term(const std::string &value)
	{
		m_sValue = value;
		m_empty = false;
	}
	Term()
	{
		m_empty = true;
	}
	~Term()
	{ }

	bool Eof() const
	{
		return m_empty;
	}
	bool Empty() const
	{
		return m_sValue.empty();
	}
	std::string Value() const
	{
		return m_sValue;
	}
};

class UrlString::Node
{
	using node_value_fn_ty = UrlString::node_value_fn_ty;
	using node_value_fn_pointer_ty = UrlString::node_value_fn_pointer_ty;

private:
	std::unique_ptr<std::string> m_value;
	node_value_fn_pointer_ty m_fn;

public:
	Node(node_value_fn_pointer_ty fn)
		: m_fn(fn)
	{ }
	Node(node_value_fn_ty &&fn)
		: m_fn(new node_value_fn_ty(std::move(fn)))
	{ }

	~Node()
	{ }

	void SetValue()
	{
		m_value.reset();

		if (!m_fn)
			return;

		std::string s = (*m_fn)();

		if (s.length())
			m_value = std::make_unique<std::string>(std::move(s));
	}
	std::unique_ptr<std::string> Svalue() const
	{
		if (!m_value)
			return {};

		return std::make_unique<std::string>(*m_value);
	}
};

#endif

