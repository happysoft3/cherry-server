
#ifndef WORK_THREAD_TASK_H__
#define WORK_THREAD_TASK_H__

#include <memory>

class WorkTaskResult;

class WorkThreadTask
{
private:
    std::unique_ptr<WorkTaskResult> m_taskResult;
    std::weak_ptr<bool> m_key;

public:
    WorkThreadTask();
    virtual ~WorkThreadTask();

private:
    virtual void runTask();
    virtual void stopService() = 0;

protected:
    template <class Ty, class ...Args>
    WorkTaskResult *makeResult(Args &&... args)
    {
        using _instTy = typename std::enable_if<std::is_base_of<WorkTaskResult, Ty>::value, Ty>::type;
        m_taskResult = std::make_unique<_instTy>(std::forward<Args>(args)...);
        return m_taskResult.get();
    }

public:
    void Run(std::weak_ptr<bool> key);
    void Stop(bool forced = false);
};

#endif

