
#ifndef C_FLUSH_H__
#define C_FLUSH_H__

#include "common/ccobject/ccobject.h"
#include <sstream>
#include <memory>
#include <future>

class CFlush : public CCObject
{
public:
	class Context;
private:
	std::ostringstream m_pendingSlot;

public:
	CFlush(Context *context = nullptr);
	~CFlush() override;

	template <class Ty>
	CFlush &operator<<(const Ty &n)
	{
		m_pendingSlot << n;
		return *this;
	}

	void Flush();
};

class CFlush::Context : public CCObject
{
public:
	Context();
	virtual ~Context();

	virtual void Put(const std::string &s) = 0;
};

#endif

