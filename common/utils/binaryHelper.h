
#ifndef BINARY_HELPER_H__
#define BINARY_HELPER_H__

#include "common/include/binaryBufferEx.h"

class BinaryHelper : public BinaryBufferEx<char>
{
private:
	uint32_t m_readPos;

public:
	BinaryHelper();
	~BinaryHelper() override;

private:
	void onBufferClean() override;

protected:	//read����
	size_t getPos() const
	{
		return m_readPos;
	}
	bool next(int expect);
	virtual int pop();
	virtual void push(int value);
	virtual int peek();
	virtual void resetSeek();
};

#endif

