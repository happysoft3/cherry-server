
#include "workThreadManager.h"
#include "workThread.h"
#include "workThreadTask.h"

WorkThreadManager::WorkThreadManager()
{ }

WorkThreadManager::~WorkThreadManager()
{ }

bool WorkThreadManager::confirmConcurrent(size_t n) const
{
    switch (n)
    {
    case 1: case 2: case 3: case 4:
        return true;

    default:
        break;
    }
    return false;
}

void WorkThreadManager::makeWorkerN(size_t n, std::shared_ptr<WorkThreadTask> task)
{
    if (!m_workers.empty())
        m_workers.clear();

    std::unique_ptr<WorkThread> worker;

    for (int i = 0 ; i < static_cast<int>(n) ; i++)
    {
        worker = std::make_unique<WorkThread>();
        worker->Run(task, m_actSignal);
        m_workers.push_back(std::move(worker));
    }
}

void WorkThreadManager::doDeploy()
{
    for (auto &worker : m_workers)
        worker->Wait();
}

void WorkThreadManager::Deploy(std::unique_ptr<WorkThreadTask> task, size_t nConcurrent)
{
    if (m_actSignal)
        return;

    m_actSignal = std::make_shared<bool>(true);
    if (!task)
        return;

    if (!confirmConcurrent(nConcurrent))
        return;

    std::shared_ptr<WorkThreadTask> sharedTask = std::move(task);

    makeWorkerN(nConcurrent, sharedTask);
    std::future<void> res = std::async(std::launch::async, [this]() { this->doDeploy(); });

    m_workerResult = std::move(res);
}

void WorkThreadManager::Stop()
{
    if (!m_workerResult.valid())
        return;

    m_actSignal.reset();
    for (auto &worker : m_workers)
        worker->PostStop();

    m_workerResult.get();
}


