
#ifndef TOKENIZER_TOKEN_H__
#define TOKENIZER_TOKEN_H__

#include <string>

class TokenizerToken
{
public:
	enum class Type
	{
		SpaceType,
		NewlineType,
		EOFType,
		NumberType,
		KeywordType,
		IdentType,
		StringType,
	};

private:
	Type m_ty;

public:
	TokenizerToken(Type ty);
	virtual ~TokenizerToken();

	Type Class() const
	{
		return m_ty;
	}
	virtual const char *const String() const
	{
		return "";
	}
	virtual int Value() const
	{
		return 0;
	}
	bool TypeExpect(Type ty)
	{
		return m_ty == ty;
	}
};

class SpaceToken : public TokenizerToken
{
public:
	SpaceToken()
		: TokenizerToken(TokenizerToken::Type::SpaceType)
	{ }

private:
	virtual const char *const String() const
	{
		return " ";
	}
};

class NewlineToken : public TokenizerToken
{
public:
	NewlineToken()
		: TokenizerToken(TokenizerToken::Type::NewlineType)
	{ }
};

class EofToken : public TokenizerToken
{
public:
	EofToken()
		: TokenizerToken(TokenizerToken::Type::EOFType)
	{ }
};

class StringStorageToken : public TokenizerToken
{
private:
	std::string m_str;

public:
	StringStorageToken(Type ty, const std::string &s);

protected:
	const char *const stringData() const
	{
		return m_str.c_str();
	}
	virtual const char *const String() const
	{
		return stringData();
	}
};

class NumericToken : public StringStorageToken
{
private:
	int m_number;

public:
	explicit NumericToken(const std::string &s);

private:
	void putNumber(const std::string &s);
	int Value() const override
	{
		return m_number;
	}
};

class KeywordToken : public TokenizerToken
{
private:
	int m_keyword;

public:
	explicit KeywordToken(int key);
	int Value() const override
	{
		return m_keyword;
	}
};

class IdentToken : public StringStorageToken
{
public:
	explicit IdentToken(const std::string &s);
};

class StringToken : public StringStorageToken
{
public:
	explicit StringToken(const std::string &s);
};

#endif

