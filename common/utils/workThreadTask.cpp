
#include "workThreadTask.h"
#include "workTaskResult.h"

WorkThreadTask::WorkThreadTask()
{ }

WorkThreadTask::~WorkThreadTask()
{ }

void WorkThreadTask::runTask()
{
    m_key.reset();
}

void WorkThreadTask::Run(std::weak_ptr<bool> key)
{
    if (!key.expired())
        return;
    if (!m_key.expired())
        return;

    m_key = key;
    for (;;)
    {
        if (m_key.expired())
            break;

        runTask();
    }
}

void WorkThreadTask::Stop(bool forced)
{
    if (m_key.expired())
        return;

    if (forced)
        m_key.reset();
    stopService();
}

