
#include "urlString.h"
#include "stringhelper.h"

using namespace _StringHelper;

static constexpr uint32_t s_eof = static_cast<uint32_t>(-1);

UrlString::UrlString()
	: BinaryHelper(), m_setValueFn(new std::function<std::string()>([this]() { return this->readValue(); }))
{ }

UrlString::~UrlString()
{ }

bool UrlString::isHexaChar(int c) const
{
	switch (c)
	{
	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9': return true;
	default: return false;
	}
}

bool UrlString::expectHexa()
{
	return isHexaChar(peek());
}

int UrlString::getHexByte(uint8_t in)
{
	static constexpr int decimal = 10;

	switch (in)
	{
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		return in - '0';
	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
		return in - 'a' + decimal;
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		return in - 'A' + decimal;
	default:
		return -1;
	}
}

bool UrlString::calcDecodeResult(uint32_t low, uint32_t high, uint8_t &outp)
{
	static constexpr int hexa = 16;

	int first = getHexByte(high);
	int second = getHexByte(low);

	if ((first < 0) || (second < 0))
		return false;

	outp = first + (second * hexa);
	return true;
}

bool UrlString::readHexaChar(uint8_t &uc)
{
	if (expectHexa())
	{
		uint32_t uFirst = pop();

		if (expectHexa())
			return calcDecodeResult(uFirst, pop(), uc);

		push(uFirst);
	}
	return false;
}

void UrlString::decodeImpl()
{
	uint32_t val = 0;
	uint8_t uc = 0;

	m_store.clear();

	for (;;)
	{
		val = pop();

		if (val == s_eof)
			break;

		switch (val)
		{
		case '%':
			if (readHexaChar(uc))
			{
				m_store.push_back(uc);
				break;
			}
		default:
			m_store.push_back(static_cast<uint8_t>(val));
		}
	}
}

template <class Container>
void UrlString::moveToStore(const Container &src, const size_t &index)
{
	if (index == -1)
		std::copy(src.cbegin(), src.cend(), std::insert_iterator<std::vector<uint8_t>>(m_store, m_store.end()));
	else
		std::copy(src.cbegin(), src.cend(), m_store.begin() + index);
}

bool UrlString::needEncodeChar(int c) const
{
	switch (c)
	{
	case ' ': case '#': case '$': case '%': case '&': /*case '\'': */case '(':
	case ')': case '*': case '+': case ',': /*case '/': */case ':': case ';':
	case '=': case '?': case '@': case '[': case ']':
	case '"': case '<': case '>': case '|': return true;

	case '\t': case '\n': case '\r': return true;

	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9': return false;
	default:
		if (c >= 0x80) return true;
		else return false;
	}
}

void UrlString::encodeImpl(uint32_t c, size_t resz)
{
	if (c == s_eof)
	{
		m_store.clear();
		if (resz != 0)			
			m_store.resize(resz);

		return;
	}

	std::string fmt = needEncodeChar(c) ? stringFormat("%%%.02X", c) : std::string(sizeof(char), c);

	encodeImpl(pop(), resz + fmt.length());
	moveToStore(fmt, resz);
}

void UrlString::encodeImplOld()
{
	m_store.clear();

	for (;;)
	{
		auto p = pop();

		if (p == s_eof)
			break;

		if (needEncodeChar(p))
		{
			moveToStore(stringFormat("%%%.02X", p));
			continue;
		}
		m_store.push_back(p);
	}
}

std::unique_ptr<UrlString::Term> UrlString::popTerm()
{
	if (m_strList.empty())
		return {};

	auto pop = std::move(m_strList.front());

	m_strList.pop_front();
	return pop;
}

void UrlString::pushTerm(std::unique_ptr<UrlString::Term> term)
{
	m_strList.push_front(std::move(term));
}

UrlString::Term *UrlString::peekTerm() const
{
	if (m_strList.empty())
		return nullptr;

	return m_strList.front().get();
}

void UrlString::splitPath()
{
	std::string buffer;

	buffer.reserve(Size());

	for (;;)
	{
		uint32_t uc = pop();

		if (uc == s_eof || uc == '/')
		{
			m_strList.push_back(std::make_unique<Term>(buffer));
			buffer.clear();
			if (uc == s_eof)
				break;

			continue;
		}
		buffer.push_back(static_cast<char>(uc));
	}
	m_strList.push_back(std::make_unique<Term>());
}

void UrlString::investigate()
{
	for (;;)
	{
		auto term = popTerm();

		if (term->Eof())
			break;

		if (!term->Empty())
		{
			Node *node = findNode(term->Value());

			if (node)
				node->SetValue();
			else
			{
				auto created = std::make_unique<Node>(m_setValueFn);

				created->SetValue();
				m_nodeMap[term->Value()] = std::move(created);
			}
		}
	}
}

UrlString::Node *UrlString::findNode(const std::string &key)
{
	auto keyIterator = m_nodeMap.find(key);

	if (keyIterator != m_nodeMap.cend())
		return keyIterator->second.get();

	return nullptr;
}

std::string UrlString::readValue()
{
	auto term = popTerm();

	if (term->Eof())
	{
		pushTerm(std::move(term));
		return {};
	}
	return term->Value();
}

void UrlString::ToNode()
{
	if (Empty())
		return;

	splitPath();
	investigate();
}

std::string UrlString::Value(const std::string &key) const
{
	auto keyIterator = m_nodeMap.find(key);

	if (keyIterator == m_nodeMap.cend())
		return {};

	auto svalue = keyIterator->second->Svalue();

	return svalue ? *svalue : "";
}

bool UrlString::HasValue(const std::string &key) const
{
	auto keyIterator = m_nodeMap.find(key);

	if (keyIterator == m_nodeMap.cend())
		return false;

	return keyIterator->second->Svalue() ? true : false;
}

void UrlString::putValueFunction(const std::string &key, UrlString::node_value_fn_ty &&fn)
{
	Node *node = findNode(key);

	if (!node)
	{
		node = new Node(std::move(fn));
		m_nodeMap[key] = std::unique_ptr<Node>(node);
	}
}



