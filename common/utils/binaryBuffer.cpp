
#include "binaryBuffer.h"
#include "stringhelper.h"
#include <windows.h>

using namespace _StringHelper;

BinaryBuffer::BinaryBuffer()
{
	m_pBuffer = &m_binBuffer;
}

BinaryBuffer::~BinaryBuffer()
{ }

void BinaryBuffer::Clear()
{
	if (m_binBuffer.size())
		m_binBuffer.clear();
	onBufferClear();
}

void BinaryBuffer::debugLineImpl(const std::string &debug)
{
	::OutputDebugStringA(toArray(debug));
}

std::string BinaryBuffer::debugLine(const std::string &debugMsg)
{
	return stringFormat("file: %s, line: %d - %s", __FILE__, __LINE__, debugMsg);
}

bool BinaryBuffer::CreateComponent()
{
	Clear();
	return true;
}
