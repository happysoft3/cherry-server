
#include "qPrintEncode.h"
#include "stringhelper.h"

using namespace _StringHelper;

struct QPrintEncode::EncodeToken
{
	std::string m_token;
	size_t m_pos;
};

struct QPrintEncode::EncodeData
{
	std::string m_encodeType;
	std::string m_encodeValue;
	std::string m_encodeSymb;
};

class QPrintEncode::Error
{
private:
	size_t m_errorPos;
	std::string m_name;

public:
	Error(EncodeToken &tok, const std::string &name)
	{
		m_errorPos = tok.m_pos;
		m_name = name;
	}

	Error(const std::string &name, size_t pos = 0)
	{
		m_errorPos = pos;
		m_name = name;
	}

	size_t Pos() const
	{
		return m_errorPos;
	}
	std::string Name() const
	{
		return m_name;
	}

	friend std::ostream &operator<<(std::ostream &outStream, const QPrintEncode::Error &err);
};

std::ostream &operator<<(std::ostream &outStream, const QPrintEncode::Error &err)
{
	outStream << stringFormat("%d- %s", err.m_errorPos, err.m_name);

	return outStream;
}

QPrintEncode::QPrintEncode()
	: BinaryHelper()
{ }

QPrintEncode::~QPrintEncode()
{ }

int QPrintEncode::hexaChar(int c) const
{
	switch (c)
	{
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		return c - 'A'+10;
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		return c - '0';

	default:
		return -1;
	}
}

int QPrintEncode::readHexaExpr(int c)
{
	int high = hexaChar(pop());
	int low = hexaChar(pop());

	if (low == -1 || high == -1)
		throw Error(stringFormat("hexa character is unmismatched, got '%c' ", static_cast<char>(c)), getPos());

	return low + (high * 16);
}

void QPrintEncode::readKeyword()
{
	std::string buffer;
	std::unique_ptr<EncodeToken> tok(new EncodeToken);

	tok->m_pos = getPos();
	for (;;)
	{
		int c = pop();

		if (c == EOF)
			throw Error(*tok, "unterminated keyword");
		if (c == '?')
		{
			tok->m_token = std::move(buffer);
			m_tokList.push_back(std::move(tok));
			break;
		}
		if (c == '=')
			c = readHexaExpr(c);

		buffer.push_back(c);
	}
}

void QPrintEncode::readSubExpr()
{
	int c = 0;

	for (;;)
	{
		c = pop();
		
		if (c == EOF)
			throw Error("unterminated sub expression", getPos());
		if (c == '=')
			break;
		if (c == '?')
		{
			readKeyword();
			continue;
		}
		auto tok = std::make_unique<EncodeToken>();

		tok->m_token.push_back(c);
		m_tokList.push_back(std::move(tok));
	}
}

bool QPrintEncode::readExpr()
{
	int c = pop();

	if (c == EOF)
		return false;

	if (c == '=')
	{
		readSubExpr();
		return true;
	}

	push(c);
	return false;
}

void QPrintEncode::makeEncodeData()
{
	auto data = std::make_unique<EncodeData>();
	std::list<std::string *> setDataList({ &data->m_encodeType, &data->m_encodeSymb, &data->m_encodeValue });
	auto iter = m_tokList.cbegin();

	for (std::string *pick : setDataList)
	{
		if (iter == m_tokList.cend())
			throw Error(**iter, "not exist attribution");
		*pick = (*iter)->m_token;
		++iter;
	}
	m_data = std::move(data);
}

//인코딩 방식과 그 내용을 얻는데에 초점을 둡니다//
bool QPrintEncode::readQPrint()
{
	try
	{
		readExpr();
		if (m_tokList.empty())
			throw Error("has no any data");
	}
	catch (const Error &err)
	{
		m_lastError = std::make_unique<Error>(std::move(err));
		return false;
	}
	makeEncodeData();
	return true;
}

bool QPrintEncode::Decode()
{
	return readQPrint();
}

std::string QPrintEncode::GetError() const
{
	if (m_lastError)
		return {};

	return stringFormat("%d- %s", m_lastError->Pos(), m_lastError->Name());
}

std::string QPrintEncode::Type() const
{
	if (m_data)
		return m_data->m_encodeType;

	return {};
}

std::string QPrintEncode::Symbol() const
{
	return m_data ? m_data->m_encodeSymb : "";
}

std::string QPrintEncode::Value() const
{
	return m_data ? m_data->m_encodeValue : "";
}
