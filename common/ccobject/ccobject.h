
#ifndef CCOBJECT_H__
#define CCOBJECT_H__

#include <memory>
#include <future>
#include <mutex>
#include <list>

class CCObject
{
protected:
	class Core
	{
	private:
		CCObject *m_owner;
		std::unique_ptr<std::promise<bool>> m_expired;

	public:
		Core(std::unique_ptr<std::promise<bool>> pr, CCObject *owner)
			: m_expired(std::move(pr)), m_owner(owner)
		{
		}

		~Core()
		{
			if (m_expired)
			{
				m_expired->set_value(true);
				m_expired.reset();
			}
		}

		CCObject *Owner() const
		{
			return m_owner;
		}
	};

	std::shared_ptr<Core> m_core;
	std::weak_ptr<Core> m_parent;
	std::unique_ptr<std::future<bool>> m_allowDeleter;

public:
	CCObject(CCObject *parent = nullptr);
	virtual ~CCObject();

protected:
	std::weak_ptr<Core> getParent() const
	{
		return m_parent;
	}
	std::weak_ptr<Core> loadCore(CCObject *other) const;
	CCObject *coreToObject(Core &core);

private:
	template <class Fn>
	struct FunctionHelper;

	template <class RetTy, class Ty>
	struct FunctionHelperBase
	{
		using ret_type = RetTy;
		using object_type = Ty;
	};

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)> : public FunctionHelperBase<Ret, Ty>
	{
	};

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)const> : public FunctionHelperBase<Ret, Ty>
	{
	};

	template <class MemberFn, class... Args>
	auto invokeTarget(CCObject *invokeTarget, MemberFn &&fn, Args&&... args)
		-> typename FunctionHelper<std::decay_t<decltype(fn)>>::ret_type		//modification on 20 June 2022-09:50
	{
		using CheckTy = typename FunctionHelper<std::remove_reference<decltype(fn)>::type>::object_type;
		using Ty = typename std::enable_if<std::is_base_of<CCObject, CheckTy>::value, CheckTy>::type;

		return (static_cast<Ty *>(invokeTarget)->*(fn))(std::forward<Args>(args)...);
	}

public:
	template <class MemberFn, class... Args>
	void Invoke(std::weak_ptr<Core> refCore, MemberFn &&fn, Args&&... args)
	{
		auto core = refCore.lock();

		if (core)
			invokeTarget(coreToObject(*core), std::forward<MemberFn>(fn), std::forward<Args>(args)...);
	}

protected:
	void makeQueueSignal(std::function<void()> &&fn);
	static void printDebugMessage(const std::string &msg);
};

namespace _event_signal_slot_private
{
	template <class... Args>
	class EventSlot;
}

template <class... Args>
class _event_signal_slot_private::EventSlot : public CCObject
{
	using slot_functionType = std::function<void(Args...)>;
private:
	slot_functionType m_slot;
	std::weak_ptr<CCObject::Core> m_refTarget;

public:
	explicit EventSlot(slot_functionType &&slot, std::weak_ptr<CCObject::Core> target)
		: CCObject(),
		m_slot(std::forward<slot_functionType>(slot))
	{
		m_refTarget = target;
	}

	~EventSlot() override
	{ }

	template <class... SlotArgs>
	void Emit(SlotArgs&&... args)
	{
		m_slot(std::forward<SlotArgs>(args)...);
	}

	inline bool Expired() const
	{
		return m_refTarget.expired();
	}
};

template <class... Args>
class EventSignal : public CCObject
{
	using slot_type = _event_signal_slot_private::EventSlot<Args...>;
	using slot_container = std::list<std::shared_ptr<slot_type>>;
	using slot_iterator = typename slot_container::iterator;

private:
	slot_container m_slots;

public:
	explicit EventSignal()
		: CCObject()
	{ }

	~EventSignal() override
	{
		//printDebugMessage(__FUNCSIG__);
	}

private:
	template <class SlotFunction>
	void addSlot(SlotFunction &&slot, CCObject *pRecv)
	{
		auto slotObject = std::shared_ptr<slot_type>(new slot_type(std::forward<SlotFunction>(slot), loadCore(pRecv)));

		m_slots.push_back(std::move(slotObject));
	}

public:
	template <class SlotFunction, class RecvInstance>
	bool Connection(SlotFunction &&slot, RecvInstance *pRecv)
	{
		static_assert(std::is_base_of<CCObject, RecvInstance>::value, "the instance must inherit CCObject");
		if (pRecv == nullptr)
			return false;

		std::weak_ptr<CCObject::Core> refptr = loadCore(pRecv);
		std::lock_guard<std::mutex> lock(m_lock);
		addSlot([callable = std::move(slot), refpt = std::move(refptr)](Args&&... args)
		{
			std::shared_ptr<CCObject::Core> recv = refpt.lock();

			if (!recv)
				return;

			(static_cast<RecvInstance *>(recv->Owner())->*callable)(std::forward<Args>(args)...);
		}, pRecv);
		return true;
	}

	template <class... SlotArgs>
	void Emit(SlotArgs&&... args)
	{
		if (!m_core)
			return;

		std::list<slot_iterator> removeSlotList;
		{
			std::lock_guard<std::mutex> lock(m_lock);
			slot_iterator travsCur = m_slots.begin();

			while (travsCur != m_slots.end())
			{
				if ((*travsCur)->Expired())
					removeSlotList.push_back(travsCur);
				else
					(*travsCur)->Emit(std::forward<SlotArgs>(args)...);
				++travsCur;
			}

			for (const auto &iter : removeSlotList)
				m_slots.erase(iter);
		}
	}

public:
	template <class... SlotArgs>
	void QueueEmit(SlotArgs&& ...args)
	{
		makeQueueSignal([args..., this]() mutable
		{
			this->Emit(std::forward<SlotArgs>(args)...);
		});
	}

private:
	std::mutex m_lock;
};

#define DECLARE_SIGNAL(SIGNAL_IDENTIFIER, ...)	\
private: \
    EventSignal<__VA_ARGS__> m_##SIGNAL_IDENTIFIER; \
public: \
    EventSignal<__VA_ARGS__> &SIGNAL_IDENTIFIER()   \
    { return m_##SIGNAL_IDENTIFIER; }    \


#endif

