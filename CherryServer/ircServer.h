
#ifndef IRC_SERVER_H__
#define IRC_SERVER_H__

#include "common/net/genericServer.h"

//이 클래스는 로비서버, 게임서버 2개로 분기되는 공통 분자입니다//
class IRCServer : public GenericServer
{
public:
    IRCServer();
    ~IRCServer() override;

};

#endif

